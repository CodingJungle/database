<?php

namespace Codingjungle\Database\Builders;

/**
 * @brief      Replace Class
 * @author     Michael S. Edwards
 * @package    Zephyr
 * @subpackage Codingjungle\Database\Builders
 */
class Replace extends Query
{
    protected $values = null;
    protected $columns = null;
    protected $bindPointer = 1;
    public $onDuplicateUpdate = false;

    /**
     * @inheritdoc
     */
    protected function compile()
    {
        $this->query = 'REPLACE INTO ' . $this->db->prefix . $this->table;
        $this->compileJoins();
        $this->query .= ' ( ' . \implode(',', $this->columns) . ' ) VALUES ';
        $values = [];
        foreach ($this->values as $value) {
            $values[] = '( ' . \implode(',', $value) . ' )';
        }
        $this->query .= \implode(',', $values);
    }

    /**
     * builds the replace data
     *
     * @param array $data
     *
     * @return Replace
     */
    public function values(array $data): \Codingjungle\Database\Builders\Replace
    {
        if (!\is_int(\key($data))) {
            $data = [$data];
        }
        $columns = \array_keys(\reset($data));
        foreach ($data as $row) {
            if ((\count(\array_diff(\array_keys($row), $columns)))) {
                throw new \InvalidArgumentException('Illegal modification of columns to replace on!');
            }
            $values = [];
            foreach ($row as $value) {
                $values[] = "?";
                $this->addBinds($this->bindPointer, $value);
                $this->bindPointer++;
            }
            $this->values[] = $values;
        }
        if ($this->columns) {
            if (\count(\array_diff($this->columns, $columns))) {
                throw new \InvalidArgumentException('Illegal modification of columns to replace on!');
            }
        }
        $this->columns = $columns;

        return $this;
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function execute()
    {
        $parent = parent::execute();
        $parent->closeCursor();

        return $this->db->lastInsertId();
    }
}
